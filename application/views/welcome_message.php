<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>rest_client</title>
	<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
	<div class="row my-4">
		<div class="col">
			<h2>coba_produk</h2>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col">
			<h3>CRUD - Product</h3>
		</div>
		<div class="col text-right">
			<button class="btn btn-success btn-sm" data-target="#modal" data-toggle="modal">TAMBAH</button>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">Nama Produk</th>
							<th scope="col">Harga</th>
							<th scope="col">Deskripsi</th>
							<th scope="col">gambar</th>
							<!-- <th scope="col" width="20">Aksi</th> -->
						</tr>
					</thead>
					<tbody id="body-table">

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm">Save changes</button>
      </div>
    </div>
  </div>
</div>





</body>
<script src="<?= base_url('asset/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('asset/js/jquery.js'); ?>"></script>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>


	<script>
		
		const APILINK = 'http://localhost/coba_rest/'

		$.ajaxSetup({
			headers: {
				'key': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxMzU2OTk5NTI0LCJuYmYiOjEzNTcwMDAwMDAsImlkX2NsaWVudCI6IjExIiwidXNlcm5hbWUiOiJTaWdpdCBCdWRpYW50byIsImVtYWlsIjoic2lnaXQ0MjNAZ21haWwuY29tIn0.WsZHLefzER8Zi_wMWiObYVqqNfxb7BrOZpNXwBE84qE',
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		});


		ambildata();

		function ambildata(){
			$.ajax({
				type : 'get',
				url : APILINK + 'produk',
				dataType : 'Json',
				success:function(data){
					var baris = '';
					for(var i = 0; i<data.data.length;i++){
						baris += '<tr>' + 
											'<td>' + data.data[i].nama_produk + '</td>' +
											'<td>' + data.data[i].harga + '</td>' +
											'<td>' + data.data[i].deskripsi + '</td>' +
											'<td><img width="100" src="'+ APILINK +'uploads/'+ data.data[i].gambar+'"></td>' +
									
								 '</tr>' ;
					}

					$('#body-table').html(baris);
				}
			});
		}


		


	</script>
</html>